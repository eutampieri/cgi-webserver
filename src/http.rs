use std::{io::Read, net::TcpStream};

pub const NUMBER_OF_HEADERS: usize = 32;
const TCP_IPV4_MSS: usize = 1460;

pub fn read_request(connection: &mut TcpStream) -> (Vec<u8>, std::ops::Range<usize>) {
    let mut bytes_read: usize = 0;

    let mut headers;
    let mut request;
    let mut buffer = Vec::with_capacity(TCP_IPV4_MSS);
    let mut body_range = 0..0;

    loop {
        buffer.extend([0].iter().cycle().take(TCP_IPV4_MSS));
        bytes_read += connection
            .read(&mut buffer[bytes_read..])
            .expect("Failed to read from socket");

        if body_range.start == 0 {
            headers = [httparse::EMPTY_HEADER; 32];
            request = httparse::Request::new(&mut headers);
            let parsing_result = request.parse(&buffer[..bytes_read]);

            if let Ok(parsing_result) = parsing_result {
                if parsing_result.is_complete() {
                    let start_of_body = parsing_result.unwrap();
                    let content_length = request
                        .headers
                        .iter()
                        .find(|x| x.name.to_lowercase() == "content-length")
                        .and_then(|x| std::str::from_utf8(x.value).ok())
                        .and_then(|x| x.parse::<usize>().ok())
                        .unwrap_or_default();
                    body_range = start_of_body..(content_length + start_of_body);
                }
            }
        }

        if bytes_read >= body_range.end {
            return (buffer, body_range);
        }
    }
}
