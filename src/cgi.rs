use std::io::Write;

#[derive(Debug)]
pub enum Error {
    NotFound,
}

#[derive(Debug)]
pub struct CgiRequest(std::process::Command);

fn convert_header(header: (String, &str)) -> (String, &str) {
    (format!("HTTP_{}", header.0.replace('-', "_")), header.1)
}

struct Paths {
    path_info: String,
    path_translated: String,
    command_path: std::path::PathBuf,
    script_name: String,
}

fn script_at(path: &std::path::Path) -> Option<std::path::PathBuf> {
    if path.exists() {
        Some(path.to_path_buf())
    } else {
        let arch = std::env::consts::ARCH;
        let mut path_with_arch = path.to_path_buf();
        let mut filename = format!(
            "{}-{}",
            path.file_stem().and_then(std::ffi::OsStr::to_str).unwrap(),
            arch
        );
        if let Some(ext) = path.extension().and_then(std::ffi::OsStr::to_str) {
            filename += ext;
        }
        path_with_arch.pop();
        path_with_arch.push(filename);
        if path_with_arch.is_file() {
            Some(path_with_arch)
        } else {
            None
        }
    }
}

fn extract_path_info(
    command_path: &mut std::path::PathBuf,
    scripts_root: &std::path::PathBuf,
) -> String {
    let mut path_info = vec![];

    while command_path.starts_with(scripts_root) {
        if let Some(p) = script_at(&*command_path) {
            *command_path = p;
            break;
        } else {
            let last = command_path.file_name().and_then(|x| x.to_str()).unwrap();
            path_info.push(last.to_owned());
            command_path.pop();
        }
    }

    url_escape::decode(
        std::path::PathBuf::from_iter(path_info.into_iter().chain(["/".to_owned()]).rev())
            .to_str()
            .unwrap(),
    )
    .to_string()
}

fn extract_script_name(command_path: &std::path::Path, scripts_root: std::path::PathBuf) -> String {
    "/".to_string()
        + command_path
            .strip_prefix(scripts_root)
            .ok()
            .and_then(|x| x.to_str())
            .unwrap_or_default()
}

fn extract_path_translated(value: &uriparse::URI, path_info: &str) -> String {
    uriparse::uri::URI::from_parts(
        value.scheme().clone(),
        value.authority().cloned(),
        path_info,
        Option::<&str>::None,
        Option::<&str>::None,
    )
    .unwrap()
    .to_string()
}

impl TryFrom<&uriparse::uri::URI<'_>> for Paths {
    type Error = Error;

    fn try_from(value: &uriparse::uri::URI<'_>) -> Result<Self, Self::Error> {
        let root = std::env::var("CGI_ROOT")
            .expect("Provide CGI_ROOT (the folder in which the scripts are located)");
        let root = std::path::PathBuf::from(root);

        let cgi_base = std::env::var("CGI_BASE").expect("Provide CGI_BASE (base URI for CGI root)");
        let cgi_base = std::path::PathBuf::from(cgi_base);

        let request_path =
            value
                .path()
                .segments()
                .iter()
                .fold(std::path::PathBuf::from("/"), |mut acc, x| {
                    acc.push(x.to_string());
                    acc
                });

        let scripts_root = if root.is_absolute() {
            root
        } else {
            let mut r = std::env::current_dir().expect("Cannot access current dir");
            r.extend(root.iter());
            r
        };

        let mut command_path = scripts_root.clone();
        command_path.extend(request_path.strip_prefix(&cgi_base));

        let path_info = extract_path_info(&mut command_path, &scripts_root);
        if command_path.is_file() {
            Ok(Self {
                path_translated: extract_path_translated(value, &path_info),
                path_info,
                script_name: extract_script_name(&command_path, scripts_root),
                command_path,
            })
        } else {
            Err(Error::NotFound)
        }
    }
}

impl TryFrom<&httparse::Request<'_, '_>> for CgiRequest {
    type Error = Error;

    fn try_from(value: &httparse::Request<'_, '_>) -> Result<Self, Self::Error> {
        let path = "http://cgi".to_owned() + value.path.unwrap_or_default();
        let path = uriparse::uri::URI::try_from(path.as_str()).unwrap();

        let paths = Paths::try_from(&path)?;

        let env_iter = value
            .headers
            .iter()
            .flat_map(|header| {
                if let Ok(value) = std::str::from_utf8(header.value) {
                    let name = header.name.to_uppercase();
                    [
                        match name.as_str() {
                            "AUTHORIZATION" => Some((
                                "AUTH_TYPE".to_owned(),
                                value.split(' ').next().unwrap_or_default(),
                            )),
                            "CONTENT-LENGTH" => Some(("CONTENT_LENGTH".to_owned(), value)),
                            "CONTENT-TYPE" => Some(("CONTENT_TYPE".to_owned(), value)),
                            "X-FORWARDED-FOR" => {
                                Some(("REMOTE_ADDR".to_owned(), value.split(',').next().unwrap()))
                            }
                            "HOST" => Some(("SERVER_NAME".to_owned(), value)),
                            _ => None,
                        },
                        Some(convert_header((name, value))),
                    ]
                } else {
                    [None, None]
                }
            })
            .flatten();
        let mut command = std::process::Command::new(&paths.command_path);
        if let Some(parent) = paths.command_path.parent() {
            command.current_dir(parent);
        }
        command
            .env_clear()
            .envs(env_iter)
            .env("GATEWAY_INTERFACE", "CGI/1.1")
            .env("PATH_INFO", paths.path_info)
            .env("PATH_TRANSLATED", paths.path_translated)
            .env(
                "QUERY_STRING",
                path.query()
                    .map(|x| x.as_str())
                    .map(|x| url_escape::decode(x).into_owned())
                    .unwrap_or_default(),
            )
            .env("REQUEST_METHOD", value.method.unwrap_or("GET"))
            .env("SCRIPT_NAME", paths.script_name)
            .env("SERVER_PORT", "80")
            .env(
                "SERVER_PROTOCOL",
                format!("HTTP/1.{}", value.version.unwrap_or_default()),
            )
            .env(
                "SERVER_SOFTWARE",
                env!("CARGO_PKG_NAME").to_string() + " " + env!("CARGO_PKG_VERSION"),
            );

        for var in &["TZ", "LANGUAGE", "LANG", "PATH"] {
            if let Ok(val) = std::env::var(var) {
                command.env(var, val);
            }
        }
        if !command.get_envs().any(|x| x.0 == "SERVER_NAME") {
            command.env("SERVER_NAME", env!("CARGO_PKG_NAME"));
        }
        Ok(Self(command))
    }
}

impl CgiRequest {
    pub fn execute(&mut self, body: Vec<u8>) -> CgiResponse {
        let mut child = self
            .0
            .stderr(std::io::stderr())
            .stdout(std::process::Stdio::piped())
            .stdin(std::process::Stdio::piped())
            .spawn()
            .expect("Failed to spawn child process");
        if !body.is_empty() {
            let stdin = child.stdin.take();
            std::thread::spawn(move || {
                stdin
                    .expect("Failed to open stdin")
                    .write_all(&body)
                    .expect("Failed to write to stdin");
            });
        }
        let output = child.wait_with_output().expect("Failed to read stdout");

        CgiResponse::from(output.stdout)
    }
}

pub struct CgiResponse {
    body: Vec<u8>,
    headers: std::collections::HashMap<String, String>,
}

impl CgiResponse {
    fn from(raw: Vec<u8>) -> Self {
        let end_of_headers = raw
            .iter()
            .enumerate()
            .fold((false, false, 0), |acc, (i, x)| {
                if acc.0 {
                    acc
                } else if x == &b'\n' && acc.1 {
                    (true, false, acc.2 - 1)
                } else if x == &b'\n' {
                    (false, true, i)
                } else {
                    (false, false, i)
                }
            });

        let (headers_range, body_range) = if end_of_headers.0 {
            (
                0..end_of_headers.2 + 1,
                (end_of_headers.2 + 3).min(raw.len())..raw.len(),
            )
        } else {
            (0..0, 0..raw.len())
        };

        let headers = std::str::from_utf8(&raw[headers_range])
            .unwrap_or_default()
            .split('\n')
            .map(|x| {
                let mut iter = x.split(": ");
                (
                    iter.next().unwrap().to_lowercase(),
                    iter.next().unwrap_or_default().to_owned(),
                )
            })
            .collect();

        Self {
            headers,
            body: raw[body_range].to_vec(),
        }
    }

    pub fn write_http_response<W: std::io::Write>(
        &self,
        writer: &mut W,
        version: u8,
    ) -> std::io::Result<()> {
        write!(writer, "HTTP/1.{} ", version)?;
        if let Some(status) = self.headers.get("status") {
            writer.write_all(status.as_bytes())?;
        } else {
            writer.write_all(b"200 OK")?;
        }
        writer.write_all(b"\r\n")?;

        if !self.headers.contains_key("content-type") {
            writer.write_all(b"Content-Type: text/plain\r\n")?
        }
        if !self.headers.contains_key("content-length") {
            write!(writer, "Content-Length: {}\r\n", self.body.len())?
        }
        for (k, v) in self.headers.iter() {
            if k == "status" {
                continue;
            }
            write!(writer, "{}: {}\r\n", k, v)?;
        }
        if !self.body.is_empty() {
            writer.write_all(b"\r\n")?;
            writer.write_all(&self.body)?;
        }
        writer.flush()?;
        Ok(())
    }
}

impl From<Error> for CgiResponse {
    fn from(value: Error) -> Self {
        match value {
            Error::NotFound => Self {
                body: b"404 Not Found".to_vec(),
                headers: std::collections::HashMap::from_iter([(
                    "status".to_owned(),
                    "404 Not Found".to_owned(),
                )]),
            },
        }
    }
}
