use std::net::SocketAddr;

use cgi::CgiResponse;

mod cgi;
mod http;

fn main() {
    let binding_address = std::env::var("BIND_SOCKET").unwrap_or("[::]:8080".to_owned());
    let binding_address: SocketAddr = binding_address.parse().expect("Invalid binding address!");
    let server = std::net::TcpListener::bind(binding_address)
        .unwrap_or_else(|_| panic!("Could not bind to {}.", binding_address));

    for mut connection in server.incoming().flatten() {
        std::thread::spawn(move || {
            let mut headers = [httparse::EMPTY_HEADER; http::NUMBER_OF_HEADERS];
            let mut request = httparse::Request::new(&mut headers);
            let (buffer, body_range) = http::read_request(&mut connection);
            request.parse(&buffer[0..body_range.start]).unwrap();

            cgi::CgiRequest::try_from(&request)
                .map(|mut x| x.execute((buffer[body_range.clone()]).to_vec()))
                .unwrap_or_else(CgiResponse::from)
                .write_http_response(&mut connection, request.version.unwrap_or_default())
                .expect("Failed to send the response!");
        });
    }
}
