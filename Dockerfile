FROM rust:latest AS builder
RUN mkdir /app
WORKDIR /app
COPY . /app
RUN cargo build --release

FROM debian:latest
RUN mkdir /app
WORKDIR /app
COPY --from=builder /app/target/release/cgi-webserver /app

ENTRYPOINT /app/cgi-webserver
ENV BIND_SOCKET=0.0.0.0:80
EXPOSE 80
