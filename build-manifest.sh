#!/bin/bash

cat <<EOF
image: ${CI_REGISTRY_IMAGE}:$1
manifests:
  - image: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}-saas-linux-medium-amd64
    platform:
      architecture: amd64
      os: linux
  - image: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}-arm64
    platform:
      architecture: arm64
      os: linux
EOF
